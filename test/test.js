const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
  });
  it('has the error page', function(done) {
    request(app)
      .get('/error')
      .expect(500, done);
  });
}); 
