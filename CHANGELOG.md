#### 0.1.0 (2019-09-30)

##### Refactors

* **api:**  reduced complexity (db00d81e)

##### Code Style Changes

* **css:**
  *  changed font (16ad098d)
  *  changed padding (80312f54)

##### Tests

* **api:**  made error page test (5ab1a310)

